<?php
namespace App\Services;

use App\Breaktime as AppBreaktime;
use App\Models\AttendanceDaily;
use App\Models\Company;
use App\Models\Breaktime;
use App\Services\GetDateService;
use Illuminate\Support\Carbon;
use App\Models\AttendanceHeader;

class AttendanceService
{
    const TIME_FORMAT = 'H:i:s';

    public function getUpdateDailyParams($params) {
        $company = Company::company();

        if ($params['attendance_class'] == AttendanceDaily::NORMAL_WORKING) {
            $dailyParams = $this->setDailyParamsNormalWorking($params, $company);
        } else if ($params['attendance_class'] == AttendanceDaily::PAID_HOLIDAYS) {
            $dailyParams = $this->setDailyParamsPaidHolidays($params, $company);
        } else {
            $dailyParams = $this->setDailyParamsDefault();
        }
        //複雑な計算を行った後、計算の為に受け取った$params（ユーザー入力値）と、
        //計算した値が入った配列$dailyParamsをmergeしてControllerに返す

        $updateDailyParams = array_merge($params, $dailyParams);

        //そのままDBに保存すれば良いシンプルな属性とは異なり、計算が必要な属性を算出する為の
        //ビジネスロジックをこのServiceで処理している
        return $updateDailyParams;
    }

    public function setDailyParamsNormalWorking($params, $company) {

        $scheduledWorkingHours = $this->getScheduledWorkingHours($company->base_time_to, $company->base_time_from);

        //課題4で追加
        $break_times = $this->getBreakTimes($params);

        $workingHours = $this->getworkingHours($params);

        $overtimeHours = $this->getOvertimeHours($workingHours, $scheduledWorkingHours);

        //複雑な計算が必要な以下の属性は、このServiceで複雑な計算をした後、配列にして、
        //大元のgetUpdateDailyParamsに返却する
        return [
            'scheduled_working_hours' => $scheduledWorkingHours,
            'working_hours' => $workingHours,
            'overtime_hours' => $overtimeHours,
            'break_times' => $break_times,
        ];
    }

    public function setDailyParamsPaidHolidays($params, $company) {
        $scheduledWorkingHours = $this->getScheduledWorkingHours($company->base_time_to, $company->base_time_from);

        $workingHours = $scheduledWorkingHours;

        $overtimeHours = date(self::TIME_FORMAT, 0);

        return [
            'scheduled_working_hours' => $scheduledWorkingHours,
            'working_hours' => $workingHours,
            'overtime_hours' => $overtimeHours,
        ];
    }

    public function setDailyParamsDefault() {
        $scheduledWorkingHours = date(self::TIME_FORMAT, 0);

        $workingHours = date(self::TIME_FORMAT, 0);

        $overtimeHours = date(self::TIME_FORMAT, 0);

        $break_times = date(self::TIME_FORMAT, 0);

        return [
            'scheduled_working_hours' => $scheduledWorkingHours,
            'working_hours' => $workingHours,
            'overtime_hours' => $overtimeHours,
            'break_times' => $break_times,
        ];
    }

    public function getScheduledWorkingHours($time_to, $time_from) {
        $from = strtotime($time_from);
        $to = strtotime($time_to);

        // 休憩時間の1時間をマイナスする
        $dif = $to - $from - strtotime($this->getDefaultBreakTime());

        return date('H:i:s', $dif);
    }

    public function getDefaultBreakTime() {
        return date('H:i:s', 3600);
    }

    //日次の勤務時間を取得する
    public function getworkingHours($params) {
        $working_time = strtotime($params['working_time']);
        $leave_time = strtotime($params['leave_time']);

        $dailyBreakTimes = 0;

        foreach ($params['break_times'] as $breaktime) {
            $break_time_from = strtotime($breaktime['break_time_from']);
            $break_time_to = strtotime($breaktime['break_time_to']);

            $dailyBreakTimes = $dailyBreakTimes + ($break_time_to - $break_time_from);
        }

        //入力時間から休憩時間を差し引き、定時時間数を$scheduled_working_hoursに格納
        $scheduled_working_hours = $leave_time - $working_time - $dailyBreakTimes;

        //$scheduled_working_hoursをH:i:s(時:分:秒)にフォーマットしてリターンする
        //課題3で追加:端数処理が日次の場合、端数処理を実行してから勤務時間をリターンする
        return $this->returnTimeForFractionSpan(date(self::TIME_FORMAT, $scheduled_working_hours));
    }

    //課題4で追加 休憩時間を取得しリターンする
    public function getBreaktimes($params){
        $dailyBreaktimes = 0;

        foreach ($params['break_times'] as $breaktime) {
            $break_time_from = strtotime($breaktime['break_time_from']);
            $break_time_to = strtotime($breaktime['break_time_to']);

            $dailyBreaktimes = $dailyBreaktimes + ($break_time_to - $break_time_from);
        }

        return date(self::TIME_FORMAT, $dailyBreaktimes);
    }

    //日次の残業時間を取得する
    public function getOvertimeHours($workingHours, $scheduledWorkingHours) {
        $time = strtotime($workingHours) - strtotime($scheduledWorkingHours);

        if ($time < 0) {
            $overtime_hours = 0;
        } else {
            $overtime_hours = $time;
        }

        //$overtime_hoursをH:i:s(時:分:秒)にフォーマットしてリターンする
        //課題3で追加:端数処理が日次の場合、端数処理を実行してから残業時間をリターンする
        return $this->returnTimeForFractionSpan(date(self::TIME_FORMAT, $overtime_hours));
    }

    public function getUpdateMonthParams($attendance_header_id) {
        $attendanceDailys = AttendanceDaily::where('attendance_header_id', '=', $attendance_header_id)->get();

        $working_days = 0;

        $scheduled_working_hours = 0;
        $overtime_hours = 0;
        $working_hours = 0;

        $getDataService = new GetDateService();

        foreach ($attendanceDailys as $attendance) {
            $working_days++;

            $scheduled_working_hours = $scheduled_working_hours + $getDataService->getHourInt($attendance->scheduled_working_hours);

            $overtime_hours = $overtime_hours + $getDataService->getHourInt($attendance->overtime_hours);

            $working_hours = $working_hours + $getDataService->getHourInt($attendance->working_hours);

        }

        return [
            'working_days' => $working_days,
            'scheduled_working_hours' => $this->amountHourFormat($scheduled_working_hours),
            'overtime_hours' => $this->amountHourFormat($overtime_hours),
            'working_hours' => $this->amountHourFormat($working_hours),
        ];
    }

    public function amountHourFormat($time) {
        $hour = $time * 3600;
        return floor($hour / 3600) . gmdate(":i:s", $hour);
    }

    public function createConfirmParams($attendanceHeader){
        if ($attendanceHeader->confirm_flag == AttendanceHeader::IS_NOT_CONFIRMED){
            $params['confirm_flag'] = AttendanceHeader::IS_CONFIRMED;
        } else {
            $params['confirm_flag'] = AttendanceHeader::IS_NOT_CONFIRMED;
        }
        return $params;
    }

    //GetDateServiceをインスタンス化し、roundDailyFractionを実行
    //端数処理が日次の場合は、端数処理をして勤務/残業時間をリターンする
    public function returnTimeForFractionSpan($time){
        $get_date_service = new GetDateService();

        return $get_date_service->roundDailyFraction($time);
    }

    public function insertDailyBreakTime($dailyBreakTimeParams, $dailyId) {
        foreach ($dailyBreakTimeParams as $params) {
            $params['attendance_daily_id'] = $dailyId;
            $dailyBreakTime = Breaktime::firstOrCreate($params);

            $dailyBreakTime->fill($params)->saveOrFail();
        }
    }
}
