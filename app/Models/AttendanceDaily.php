<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\AuthorObservable;

class AttendanceDaily extends Model
{
    use AuthorObservable;

    protected $table = 'attendance_daily';

    const NORMAL_WORKING = 0;
    const PAID_HOLIDAYS = 1;
    const ABSENT_WORKING = 2;

    protected $fillable = [
        'attendance_header_id',
        'work_date',
        'attendance_class',
        'working_time',
        'leave_time',
        'break_time_from',
        'break_time_to',
        'memo',
        'scheduled_working_hours',
        'overtime_hours',
        'working_hours',
        'break_times',
    ];

    //attendance_dailyテーブルから、idが一致するレコードを配列に変換し、column_key=nullなので値全体を返却し、
    //work_dateをキーとした配列が取れる
    public static function monthOfDailys($attendance_header_id) {
        return array_column(self::where('attendance_header_id', '=', $attendance_header_id)->get()->toArray(), null, 'work_date');
    }

    //課題4で追加
    public function breaktimes() {
        return $this->hasMany('App\Models\Breaktime', 'attendance_daily_id');
    }
}
