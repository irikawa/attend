<?php

namespace App\Http\Controllers\Admin;

use App\Breaktime as AppBreaktime;
use App\Exports\UsersExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\AttendanceRequest;
use App\Http\Requests\AttendanceConfirmRequest;
use App\Http\Requests\BreaktimeRequest;
use App\Http\Resources\AttendanceDailyResource;
use App\Models\AttendanceDaily;
use App\Models\AttendanceHeader;
use App\Models\Company;
use App\Models\User;
use App\Models\Breaktime;
use App\Models\Department;
use App\Models\DepartmentMember;
use App\Services\AttendanceService;
use App\Services\GetDateService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class AttendanceHeaderController extends Controller
{
    public function index(Request $request) {

        //Servicesクラスのインスタンスを作成
        $getDateService = new GetDateService();

        //変数dateに、Y-mの形でフォーマットされた月初の値を代入（2020-06）
        $date = $getDateService->createYearMonthFormat($request->year_month);

        //変数dateForSearchにY-m-dの形でフォーマットされた値を代入（2020-06-01）
        $dateForSearch = $date->format('Y-m-d');

        //変数queryにUserモデルからid昇順にソートした値を代入
        $query = User::orderBy('users.id');

        //変数usersにUserモデルで定義されたテーブル結合する関数を実行
        $users = $query->ledftJoinAttendanceHeader($dateForSearch);

        //テーブルを結合したデータ$usersと2020-06にフォーマットされた$dateをviewファイルと共にreturnする
        return view('admin.attendance_header.index')->with([
            'users' => $users,
            'date' => $date->format('Y-m'),
        ]);
    }

    public function show($user_id, $yearMonth) {

        $getDateService = new GetDateService();

        $date = $getDateService->createYearMonthFormat($yearMonth);

        $attendance = AttendanceHeader::firstOrNew(['user_id' => $user_id, 'year_month' => $date]);

        $atendanceDaily = AttendanceDaily::monthOfDailys($attendance->id);

        $daysOfMonth = $getDateService->getDaysOfMonth($date->copy());

        $company = Company::company();

        return view('admin.attendance_header.show')->with([
            'attendance' => $attendance,
            'atendanceDaily' => $atendanceDaily,
            'daysOfMonth' => $daysOfMonth,
            'date' => $date->format('Y-m'),
            'company' => $company,
        ]);
    }

    public function update(AttendanceRequest $request) {

        $attendanceService = new AttendanceService();
        $getDateService = new GetDateService();
        $date = $getDateService->createYearMonthFormat($request->year_month);

        try {
            DB::transaction(function () use ($request, $attendanceService, $date) {

                // 抽出①を実行
                $attendanceHeader = AttendanceHeader::firstOrCreate(['user_id' => $request->user_id, 'year_month' => $date]);

                // 労働時間計算処理(日)
                $requestParams = $request->validated();
                $updateDailyParams = $attendanceService->getUpdateDailyParams($requestParams);

                // 更新処理①を実行
                $attendanceDaily = AttendanceDaily::firstOrNew(['attendance_header_id' => $attendanceHeader->id, 'work_date' => $request->work_date]);
                $attendanceDaily->fill($updateDailyParams)->saveOrfail();

                // 課題4 休憩時間の削除処理
                Breaktime::where('attendance_daily_id', $attendanceDaily->id)->delete();
                // 課題4 休憩時間の登録処理
                $dailyBreakTimeParams = $requestParams['break_times'];
                $attendanceService->insertDailyBreakTime($dailyBreakTimeParams, $attendanceDaily->id);

                // 労働時間計算処理(月)
                $updateMonthParams = $attendanceService->getUpdateMonthParams($attendanceHeader->id);

                // 更新処理②を実行
                $attendanceHeader->fill($updateMonthParams)->saveOrFail();

            });
        } catch (\Exception $e) {
            report($e);
            session()->flash('flash_message', '更新が失敗しました');
        }

        return redirect(route('admin.attendance_header.show', ['user_id' => $request->user_id, 'year_month' => $date]));
    }

    public function destroy($user_id, $year_month, $work_date) {
        $attendanceService = new AttendanceService();

        // 抽出①を実行
        $getDateService = new GetDateService();
        $date = $getDateService->createYearMonthFormat($year_month);
        $attendanceHeader = AttendanceHeader::firstOrCreate(['user_id' => $user_id, 'year_month' => $date]);

        // 更新処理④
        Attendancedaily::where(['attendance_header_id' => $attendanceHeader->id, 'work_date' => $work_date])->delete();

        // 労働時間計算処理(月)
        $updateMonthParams = $attendanceService->getUpdateMonthParams($attendanceHeader->id);

        // 更新処理②を実行
        $attendanceHeader->fill($updateMonthParams)->saveOrFail();

        return redirect(route('admin.attendance_header.show', ['user_id' => $user_id, 'year_month' => $date]));
    }


    public function ajaxGetAttendanceInfo(Request $request) {
        $attendanceDaily = AttendanceDaily::findOrNew($request->id);
        return AttendanceDailyResource::make($attendanceDaily);
    }

    //課題2 確定処理の追加
    public function confirm(AttendanceConfirmRequest $request) {
        //AttendanceServicesクラスのインスタンスを作成
        $attendanceService = new AttendanceService();

        //抽出①を実行
        //GetDataServiceクラスのインスタンスを作成
        $getDataService = new GetDateService();
        $date = $getDataService->createYearMonthFormat($request->year_month);
        $attendanceHeader = AttendanceHeader::firstOrCreate(['user_id' => $request->user_id, 'year_month' => $date]);

        //変数confirmParamsに、対象userのconfirmパラメータを作成する
        $confirmParams = $attendanceService->createConfirmParams($attendanceHeader);

        DB::transaction(function() use ($attendanceHeader, $confirmParams) {
            $attendanceHeader->fill($confirmParams)->saveOrFail();
        });

         return redirect(route('admin.attendance_header.show',
             ['user_id' => $request->user_id,
             'year_month' => $request->year_month]));

    }

    //課題5で追加
    public function export(Request $request) {
        $getDateService = new GetDateService();
        $date = $getDateService->createYearMonthFormat($request->year_month);
        $dateForSearch = $date->format('Y-m-d');

        //①残業基準時間を超える勤怠ヘッダを取得
        $basis_time =  $request->overtimes_basis_time;
        $attendanceHeaders = AttendanceHeader::where('year_month', $dateForSearch)
        ->where('overtime_hours', '>', $basis_time)
        ->get();
        $count = count($attendanceHeaders);

        //②勤怠ヘッダ(①)からユーザー及び部署名を取得
        $matchedDepartmentArray = [];
        for($i = 0; $i < $count; $i++) {
            $userId = $attendanceHeaders[$i]->user_id;
            $departmentArray = DepartmentMember::getDepartments($userId);
            $departmentName = $departmentArray[0]['name'];
            array_push($matchedDepartmentArray, $departmentName);
        }

        //③部署名(②)の重複をチェックし、部署あたりの数を取得
        $departmentCount = array_count_values($matchedDepartmentArray);
        $depCount = count($departmentCount);

        //④③を連想配列ではなく普通の配列に変換
        $index = 0;
        $departmentCountPlaneArray = [];

        foreach($departmentCount as $key => $value) {
            $index += 1;
            $departmentCountPlaneArray[$index-1][0] = $key;
            $departmentCountPlaneArray[$index-1][1] = $value;
        }

        //⑤UsersExportクラスに④に渡しインスタンスを生成
        $export = new UsersExport($departmentCountPlaneArray);
        return Excel::download($export, 'users.xlsx');
    }
}
