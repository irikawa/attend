<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use App\Traits\AuthorObservable;

class Breaktime extends Model
{
    protected $fillable = [
        'attendance_daily_id',
        'break_time_from',
        'break_time_to',
    ];
}
