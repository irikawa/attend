<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class BreaktimeRule implements Rule
{
    private $attribute;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($attribute)
    {
        $this->attribute = $attribute;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $from = strtotime($this->attribute['working_time']);
        $to = strtotime($this->attribute['leave_time']);

        $working_time = $to - $from;

        $breakTimeAmount = 0;

        $breakTimeFrom = [];
        $breakTimeTo = [];

        foreach ($value as $breaktime) {
            if ($breaktime['break_time_from'] >= $breaktime['break_time_to']) {
                return false;
            } else {
                $breakTimeFrom[] = $breaktime['break_time_from'];
                $breakTimeTo[] = $breaktime['break_time_to'];

                $breakTimeAmount = $breakTimeAmount + (strtotime($breaktime['break_time_to']) - strtotime($breaktime['break_time_from']));
            }
        }

        //重複していないか
        if (!$this->isUniqueArray($breakTimeFrom) || !$this->isUniqueArray($breakTimeTo)) {
            return false;
        }

        //休憩が労働時間を上回っていないか
        if ($breakTimeAmount >= $working_time) {
            return false;
        }

        // $from = $attribute['break_times']['break_time_from'];
        // $to = $attribute['break_times']['break_time_to'];

        //休憩時間が複数入力される
        // foreach ($value as $data) {
        //     // $fromTime = $data['break_time_from'];
        //     // $toTime = $data['break_time_to'];
        // }

        return true;
    }

    public function isUniqueArray($target_array) {
        $uniqueArray = array_unique($target_array);

        if (count($uniqueArray) == count($target_array)) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return '休憩時間に誤りがあります。';
    }
}
