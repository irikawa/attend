<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\AuthorObservable;

class Company extends Model
{
    use AuthorObservable;

    const TIME_FRACTION_LIST = [
        '1' => 'なし',
        '15' => '15分',
        '30' => '30分'
    ];

    const TIME_FRACTION_VALUES = [1, 15, 30];

    //課題3で追加
    const FRACTION_FLAG_LIST = [
        '0' => '月',
        '1' => '日'
    ];

    const FRACTION_FLAG_MONTH = '0';
    const FRACTION_FLAG_DAILY = '1';

    const FRACTION_FLAG_VALUES = [0, 1];

    protected $table = 'company';

    protected $fillable = [
        'base_time_from',
        'base_time_to',
        'time_fraction',
        'fraction_flag',
    ];

    public static function company() {
        return self::findOrFail(1);
    }

    public function getBaseTimeFromAttribute($value)
    {
        return substr($value, 0, -3);
    }

    public function getBaseTimeToAttribute($value)
    {
        return substr($value, 0, -3);
    }

}
