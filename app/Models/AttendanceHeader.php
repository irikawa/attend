<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\AuthorObservable;

class AttendanceHeader extends Model
{
    use AuthorObservable;

    CONST FRACTION_1 = 1;
    CONST FRACTION_15 = 15;
    CONST FRACTION_30 = 30;

    //課題2 確定処理のフラグを定数宣言
    CONST IS_CONFIRMED = 1;
    CONST IS_NOT_CONFIRMED = 0;

    protected $table = 'attendance_header';

    protected $fillable = [
        'user_id',
        'year_month',
        'working_days',
        'scheduled_working_hours',
        'overtime_hours',
        'working_hours',
        'confirm_flag',
    ];

    public function attendancDailies() {
        return $this->hasMany('App\Models\AttendanceDaily', 'attendance_header_id');
    }

    public function user() {
        return $this->belongsTo('App\Models\User');
    }
}
