@extends('layouts.app')

@section('addCss')
    <link rel="stylesheet" href="{{ asset('/css/admin/form.css') }}">
@endsection

@section('content')
    <div class="container company validation-url" id="attendance-info-url" data-url="{{ route('admin.attendance_header.ajax_get_attendance_info') }}" data-base_time_from="{{ $company->base_time_from }}" data-base_time_to="{{ $company->base_time_to }}">
        <div class="row pb-3 d-flex justify-content-between col-12">
            <form method="GET" action="{{ route('admin.attendance_header.index') }}">
                @csrf
                <input type="hidden" name="year_month" value="{{ $date }}">
                <button type="submit" class="d-none" id="year_month_submit"></button>
                <div class="col-12">
                    <div class="back-index click-text">戻る</div>
                </div>
            </form>
        </div>

        <div class="row pb-3">
            <div class="float-right">
                <form method="GET" action="{{ route('admin.attendance_header.confirm') }}">
                    @csrf
                    <input type="hidden" name="year_month" value="{{ $date }}">
                    <input type="hidden" name="user_id" value="{{ $attendance->user_id }}">
                    <button type="submit" class="btn btn-secondary px-5">{{ AttendanceHelper::isConfirmed($attendance->confirm_flag) }}</button>
                </form>
            </div>
        </div>


        <div class="row pb-3 d-flex">
            <div class="col-md-4 mt-3">
                <div data-action="{{ route('admin.attendance_header.show', ['user_id' => $attendance->user_id, 'year_month' => 'year_month']) }}" id="year_month_url">
                    <input type="text" class="monthPick" id="year_month" name="year_month" value="{{ $date }}">
                    <input type="submit" class="d-none" id="year_month_submit">
                </div>
            </div>

            <div class="col-md-4 justify-content-end mt-3">
                <div>
                    <h2>{{ $attendance->user->last_name }}{{ $attendance->user->first_name }}</h2>
                </div>
            </div>
        </div>

        @if ($attendance->confirm_flag == \App\Models\AttendanceHeader::IS_CONFIRMED)
            <div class="col-12 pr-0 pl-0">
                <div class="alert alert-danger">
                    確定済みのため操作することが出来ません。
                </div>
            </div>
        @endif

        <table class="table table-bordered {{ $attendance->confirm_flag == \App\Models\AttendanceHeader::IS_CONFIRMED ? 'none-events' : '' }}">
            <thead class="bg-info">
            <tr>
                <th>勤務日数</th>
                <th>所定内労働時間</th>
                <th>残業時間</th>
                <th>総労働時間</th>
            </tr>
            </thead>
            <tbody>
            <tr class="bg-white">
                <th class="text-center">{{ AttendanceHelper::daysFormat($attendance->working_days) }}日</th>
                <th class="text-center">{{ AttendanceHelper::timeFormat($attendance->scheduled_working_hours) }}</th>
                <th class="text-center">{{ AttendanceHelper::timeFormat($attendance->overtime_hours) }}</th>
                <th class="text-center">{{ AttendanceHelper::timeFormat($attendance->working_hours) }}</th>
            </tr>
            </tbody>
        </table>
        <table class="table table-bordered">
            <thead class="bg-info">
            <tr>
                <th>日付</th>
                <th></th>
                <th>区分</th>
                <th>勤務</th>
                <th>休憩時間</th>
                <th>所定内労働</th>
                <th>残業</th>
                <th>総労働時間</th>
            </tr>
            </thead>
            <tbody>
            @foreach($daysOfMonth as $day)
                <tr class="bg-white dateInfo">
                    <th class="text-right dialog date_info work_date click-text" data-date_info="{{ $date . '-' . $day['day'] . '(' . $day['dayOfWeek'] . ')' }}" data-work_date="{{ $day['work_date'] }}">{{ $day['day'] }}日</th>
                    <th class="text-center">{{ $day['dayOfWeek'] }}</th>
                    @if (count($atendanceDaily) > 0)
                        @if (isset($atendanceDaily[$day['work_date']]))
                            <th class="text-center attendance_class memo id"  data-id="{{ $atendanceDaily[$day['work_date']]['id'] }}">
                                {{ AttendanceHelper::attendanceClass($atendanceDaily[$day['work_date']]['attendance_class']) }}
                            </th>
                            <th class="text-center working_time leave_time">
                                {{ AttendanceHelper::timeFormat($atendanceDaily[$day['work_date']]['working_time'])  }}
                                ~
                                {{ AttendanceHelper::timeFormat($atendanceDaily[$day['work_date']]['leave_time']) }}
                            </th>
                            <th class="text-center break_time_from break_time_to">
{{ AttendanceHelper::timeFormat($atendanceDaily[$day['work_date']]['break_times'])  }}
                            </th>
                            <th class="text-right scheduled_working_hours">
                                {{ AttendanceHelper::timeFormat($atendanceDaily[$day['work_date']]['scheduled_working_hours']) }}
                            </th>
                            <th class="text-right overtime_hours">
                                {{ AttendanceHelper::timeFormat($atendanceDaily[$day['work_date']]['overtime_hours']) }}
                            </th>
                            <th class="text-right working_hours">
                                {{ AttendanceHelper::timeFormat($atendanceDaily[$day['work_date']]['working_hours']) }}
                            </th>
                        @else
                            <th class="text-center"></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        @endif
                    @else
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="閉じる">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div><!-- /.modal-header -->
                <form method="GET" action="{{ route('admin.attendance_header.update') }}" id="modal-form">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="alert-danger d-none col-12">
                                <ul>

                                </ul>
                            </div>
                        </div>
                        <input type="hidden" name="user_id" value="{{ $attendance->user_id }}">
                        <input type="hidden" name="year_month" value="{{ $date }}">
                        <input type="hidden" name="work_date" value="" id="work_date">
                        <div class="form-group row">
                            <label for="attendance_class" class="col-md-4 col-form-label text-right">
                                区分
                            </label>
                            <div class="col-md-8">
                                <div class="form-inline">
                                    <select name="attendance_class" class="form-control" id="attendance_class">
                                        <option value="0">通常勤務</option>
                                        <option value="1">有給休暇</option>
                                        <option value="2">欠勤</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="working_time" class="col-md-4 col-form-label text-right">
                                出勤
                            </label>
                            <div class="col-md-8">
                                <div class="form-inline">
                                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                        <input id="working_time" size="8" type="time" name="working_time" class="form-control" value="{{ $company->base_time_from }}">
                                    </div>
                                    <div class="input-group mb-2 mr-sm-2  ml-sm-2 mb-sm-0">
                                        〜
                                    </div>
                                    <div class="input-group mb-2 ml-sm-2 mb-sm-0">
                                        <input id="leave_time" type="time" size="8" name="leave_time" class="form-control" value="{{ $company->base_time_to }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row" id="breaktime">
                            <label class="col-md-4 col-form-label text-right">
                                休憩
                            </label>
                            <div class="col-md-8 d-flex mt-2">
                                休憩時間を追加する
                                <div class="mb-2 ml-sm-2 mb-sm-0">
                                        <button type="button" class="btn btn-success rounded-circle p-0 mb-2" style="width:1.5rem;height:1.5rem;" id="plus">＋</button>
                                </div>
                            </div>
                            <div class="col-md-8 offset-md-4 mb-2 " id="breaktime_form" style="display: none;">
                                <div class="form-inline">
                                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                        <input id="break_time_from" size="8" type="time" name="" class="form-control" value="12:00">
                                    </div>
                                    <div class="input-group mb-2 mr-sm-2  ml-sm-2 mb-sm-0">
                                        〜
                                    </div>
                                    <div class="input-group mb-2 ml-sm-2 mb-sm-0">
                                        <input id="break_time_to" type="time" size="8" name="" class="form-control" value="13:00">
                                    </div>

                                </div>
                            </div>
                        </div>





                        <div class="form-group row">
                            <label for="memo" class="col-md-4 control-label col-form-label text-right">
                                メモ
                            </label>
                            <div class="col-md-8">
                                <textarea class="field-textarea" id="memo" class="form-control" name="memo"></textarea>
                            </div>
                        </div>

                    </div><!-- /.modal-body -->
                    <div class="modal-footer">
                        <a data-url="{{ route('admin.attendance_header.delete', ['user_id' => $attendance->user_id, 'year_month' => $date, 'work_date' => 'work_date']) }}" class="btn btn-secondary" id="delete-url">未入力に戻す</a>
                        <button type="button" class="btn btn-primary" id="attendance_submit">変更を保存</button>
                    </div><!-- /.modal-footer -->
            </div><!-- /.modal-content -->
            </form>
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection
@section('addJs')
    <script src="{{ asset('js/attendanceForm.js') }}"></script>
    <script src="{{ asset('js/addBreaktimeArea.js') }}"></script>
@endsection
