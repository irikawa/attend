<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DailyBreaktimeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'attendance_daily_id' => $this->attendance_daily_id,
            'break_time_from' => $this->break_time_from,
            'break_time_to' => $this->break_time_to
        ];
    }
}
