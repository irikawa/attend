<?php

namespace App\Services;

use App\Models\AttendanceHeader;
use App\Models\Company;
use Illuminate\Support\Carbon;

class GetDateService
{
    public static function getNowYearMonth() {
        return Carbon::now()->format('Y-m');
    }

    public function checkYearMonthFormat($yearMonth) {
        //hasFormatで日付が'Y-m'に一致しているかをチェックする
        if (Carbon::hasFormat($yearMonth, 'Y-m')) {
            return true;
        } else {
            return false;
        }
    }

    public function createYearMonthFormat($yearMonth) {
        //FormatCheckがtrueの場合、$date変数をリターンする
        if ($this->checkYearMonthFormat($yearMonth)) {
            $date = Carbon::createFromFormat('Y-m', $yearMonth)->startOfMonth();
        } else {
            $date = Carbon::now()->startOfMonth();
        }

        return $date;
    }

    public function getDaysOfMonth($date) {
        $endDate = $date->copy()->endOfMonth()->day;
        $dateOfMonth = [];

        for ($i = 0; $i < $endDate; $i++, $date->addDay()) {
            $dateOfMonth[] =
                [
                    'day' => $date->day,
                    'dayOfWeek' => $this->getDayOfWeek($date->dayOfWeek),
                    'work_date' => $date->toDateString()
                ];
        }

        return $dateOfMonth;
    }

    public function getDayOfWeek($dayOfWeekNumber) {
        $dayOfWeek = [
            '日', '月', '火', '水', '木','金', '土'
        ];

        return $dayOfWeek[$dayOfWeekNumber];
    }

    public static function diffInHours($time_to, $time_from) {
        $from = new carbon($time_from);
        $to = Carbon::parse($time_to);

        return $from->diffInHours($to);
    }

    public function getHourInt($time) {
        //companyをインスタンス化
        $company = Company::company();
        //引数$timeを元に日時を作成し、$carbonに格納
        $carbon = Carbon::create($time);

        //作成した日付の分の部分と$companyをceilTime関数で処理したインスタンスを$minに格納
        //ceilTime関数は、$companyが持つtime_fractionの値に応じて端数処理を切り替えてくれる
        if ($company->fraction_flag == Company::FRACTION_FLAG_MONTH) {
            $min = $this->ceilTime($carbon->minute, $company);
        } else {
            $min = $carbon->minute;
        }

        //時間と端数処理された分数をリターンする
        return $carbon->hour + ($min / 60);
    }

    // 端数処理(なし/15/30)の設定に基づき算出した端数を返す
    public function ceilTime($time, $company) {
        $ceil = $company->time_fraction;
        if ($ceil == AttendanceHeader::FRACTION_1) {
            $return = $time;
        } else if($ceil == AttendanceHeader::FRACTION_15) {
            $return = $this->settingFraction15($time);
        } else if($ceil == AttendanceHeader::FRACTION_30) {
            $return = $this->settingFraction30($time);
        }

        return $return;
    }

    // 端数処理が15分単位の場合
    public function settingFraction15($time) {

        if ($time >= 0 && $time < 15) {
            $res = 0;
        } else if ($time >= 15 && $time < 30) {
            $res = 15;
        } else if ($time >= 30 && $time < 45) {
            $res = 30;
        } else if ($time >= 45) {
            $res = 45;
        }

        return $res;
    }

    // 端数処理が30分単位の場合
    public function settingFraction30($time) {
        if ($time >= 0 && $time < 30) {
            $res = 0;
        } else if ($time >= 30) {
            $res = 30;
        }

        return $res;
    }

    //端数処理が日次の場合、端数処理を実行する
    public function roundDailyFraction($time) {
        $company = Company::company();
        $carbon = Carbon::create($time);

        if($company->fraction_flag == Company::FRACTION_FLAG_DAILY){
            $minute = $this->ceilTime($carbon->minute, $company);
        } else {
            $minute = $carbon->minute;
        }

        $rounded_time = $carbon->setMinutes($minute)->format('H:i:s');

        return $rounded_time;
    }
}
