const NORMAL_WORKING = '0';
const PAID_HOLIDAYS = '1';
const ABSENT_WORKING = '2';

const BASE_BREAK_TIME_FROM = '12:00:00'
const BASE_BREAK_TIME_TO = '13:00:00'

var companyBaseTimeFrom = isset($('.company').data('base_time_from')) ? $('.company').data('base_time_from') : '';
var companyBaseTimeTo = isset($('.company').data('base_time_to')) ? $('.company').data('base_time_to') : '';

var getAttendanceInfoUrl = $('#attendance-info-url').data('url');
$('#department-index').removeAttr('data-url');

$(function() {
    $(".dialog").click(function() {
        var parent = $(this).parent();

        var id = parent.find('.id').data('id');

        if(isset(id)) {
            $.ajax({
                type:'GET',
                url: getAttendanceInfoUrl,
                dataType:'json',
                data: {id: id}
            }).done(function (result){
                let data = result.data;

                var attendance_class = isset(data.attendance_class) ? data.attendance_class : NORMAL_WORKING;
                var working_time = isset(data.working_time) ? data.working_time : companyBaseTimeFrom;
                var leave_time = isset(data.leave_time) ? data.leave_time : companyBaseTimeTo;

                //課題4で削除
                // var break_time_from = isset(data.break_time_from) ? data.break_time_from : BASE_BREAK_TIME_FROM;
                // var break_time_to = isset(data.break_time_to) ? data.break_time_to : BASE_BREAK_TIME_TO;

                //課題4で追加
                // var break_time_from = isset(data.break_times.break_time_from) ? data.break_time_from : BASE_BREAK_TIME_FROM;
                // var break_time_to = isset(data.break_times.break_time_to) ? data.break_time_to : BASE_BREAK_TIME_TO;

                var break_times_count = data.break_times.length;
                var breakTimes = data.break_times;

                if(break_times_count > 0) {
                    $(breakTimes).each(function(index, element) {
                        let breaktime_form = '#breaktime_form';
                        let lastBreaktimeForm = $('#breaktime').last(breaktime_form);
                        if(index === 0) {
                            $(breaktime_form).show();
                            addArrayIndexToEditForm(index);
                            $('#break_time_from').attr('value',element.break_time_from);
                            $('#break_time_to').attr('value',element.break_time_to);
                        } else {
                            form = $(breaktime_form).clone(true);
                            $(lastBreaktimeForm).append(form);
                            addArrayIndexToEditForm(index);
                            $('#break_time_from').attr('value',element.break_time_from);
                            $('#break_time_to').attr('value',element.break_time_to);
                        }
                    })
                }



                var memo = isset(data.memo) ? data.memo : '';

                $('#attendance_class').val(attendance_class);
                $('#working_time').val(working_time);
                $('#leave_time').val(leave_time);

                $('#break_time_from').val(break_time_from);
                $('#break_time_to').val(break_time_to);

                $('#memo').val(memo);
            }).fail(function(jqXHR,textStatus,errorThrown){
                console.log("XMLHttpRequest : " + XMLHttpRequest.status);
                console.log("textStatus     : " + textStatus);
                console.log("errorThrown    : " + errorThrown.message);
                alert('ajax通信に失敗しました');
            });
        } else {

            var attendance_class =  NORMAL_WORKING;
            var working_time =  companyBaseTimeFrom;
            var leave_time =  companyBaseTimeTo;
            var break_time_from =  BASE_BREAK_TIME_FROM;
            var break_time_to = BASE_BREAK_TIME_TO;
            var memo =  '';

            $('#attendance_class').val(attendance_class);
            $('#working_time').val(working_time);
            $('#leave_time').val(leave_time);

            $('#break_time_from').val(break_time_from);
            $('#break_time_to').val(break_time_to);

            $('#memo').val(memo);
        }


        let dateInfo = parent.find('.date_info').data('date_info');
        let work_date = parent.find('.work_date').data('work_date');

        let replace = $('#delete-url').data("url").replace('work_date', work_date);

        $('#work_date').val(work_date);

        $('#delete-url').attr("href", replace) ;

        $('.modal-title').text(dateInfo);
        removeErrorElement();
        $(".modal").modal("show");
    });
});

$(function() {
    $('#attendance_submit').click(function() {
        let form = $('#modal-form');
        modalAjaxPost(form);
    });
});

function addArrayIndexToEditForm(index) {
    $(lastBreaktimeForm).find('#break_time_from').attr('name', "break_times[" + (index) + "][break_time_from]");
    $(lastBreaktimeForm).find('#break_time_to').attr('name', "break_times[" + (index) + "][break_time_to]");
}

