<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBreaktimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('breaktimes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('attendance_daily_id')->unsigned();
            $table->foreign('attendance_daily_id')->references('id')
                ->on('attendance_daily')->onDelete('cascade');
            $table->time('break_time_from');
            $table->time('break_time_to');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('breaktimes');
    }
}
