<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnBreaktimeFromAttendanceDaily extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attendance_daily', function (Blueprint $table) {
            $table->dropColumn('break_time_from');
            $table->dropColumn('break_time_to');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attendance_daily', function (Blueprint $table) {
            $table->time('break_time_from');
            $table->time('break_time_to');
        });
    }
}
