<?php

namespace App\Exports;

use App\Models\User;
use App\Models\Department;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Facades\Log;

class UsersExport implements FromArray,WithHeadings
{
    protected $array;

    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct(array $array)
    {
        $this->array = $array;
    }

    public function array(): array
    {
       return $this->array;

    }

    public function headings():array
    {
        return [
            '部署名',
            '人数'
        ];
    }
}
